Spring Boot with GraphQL
=========================

Based on [https://github.com/graphql-java-kickstart/graphql-spring-boot](https://github.com/graphql-java-kickstart/graphql-spring-boot)

## Run

```bash
docker-compose up -d
mvn clean spring-boot:run
```

Voyage: [http://localhost:8080/voyager](http://localhost:8080/voyager)

GraphiQL: [http://localhost:8080/graphiql](http://localhost:8080/graphiql)

MongoDB UI: [http://localhost:8081/db/tweets/](http://localhost:8081/db/tweets/)

## Schema

```graphql
type Tweet {
    id: String!
    body: String!
    date: Date!
    user: User!
    userId: String!
    likes: Int
    likedBy: [User]
}

type User {
    id:String!
    name:String!
    tweets: [Tweet]!
}

scalar Date

type Query {
    Tweet(id: String!): Tweet
    Tweets(range: TimeRange!, userId: String): [Tweet]
    Users:[User]
    me: User
}

input TimeRange {
    from: Date!
    to: Date!
}

type Mutation {
    createTweet (body: String!): Tweet
    likeTweet (tweet_id: String!): Tweet
}

type Subscription{
    Time: String
    Tweets: Tweet
    TweetTexts: String
}
```


Sample Queries
==============

```graphql
query AllUsers {
  Users {
    name
    id
  }
}

query AllUsersWithTweets {
  Users {
    name
    id
    tweets {
      body
    }
  }
}

query GetSingleTweet {
  Tweet(id: "ID") {
    body
    date
    user {
      name
    }
    likes
  }
}

query TweetsInRange {
  Tweets(range: {from: "2019-10-10", to: "2020-10-10"}) {
    date
    id
    likes
    user {
			name
    }
  }
}

query TweetsInRangeForUser {
  Tweets(range: {from: "2019-10-10", to: "2020-10-10"}, userId: "1e91fd4e-ca95-4eb0-8576-e381d579808d") {
    date
    id
    likes
    user{
      id
    }
  }
}

mutation CreateTweet {
  createTweet(body: "Hi CCSP Team!") {
    id
  }
}

mutation LikeIt {
  likeTweet(tweet_id: "62dfe14c-bfdb-4410-a037-1da6cb4b08f1") {
    date
    id
    likes
    likedBy {
      name
    }
  }
}

subscription Time{
  Time
}

subscription Tweets{
  Tweets{
    body
  }
}
```