package com.example.demo


import org.assertj.core.api.AbstractCharSequenceAssert
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.json.BasicJsonTester
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.postForEntity
import org.springframework.http.ResponseEntity


class GraphQlTest : SpingBootTest() {

    @Autowired
    lateinit var client: TestRestTemplate

    val json: BasicJsonTester = BasicJsonTester(this::class.java);

    @Test
    fun canQueryTweetById() {

        val tweet = aTweet("tweet")

        val query = """
            query TweetsInRange {
                Tweets(range: {from: "2000-10-10", to: "2030-10-10"}) {
                    date
                    id
                    body
                }
            }
        """.trimIndent()

        val response = client.graphQl("TweetsInRange", query)

        response.assertJson("$.data.Tweets[0].body").isEqualTo("tweet")
        response.assertJson("$.data.Tweets[0].id").isEqualTo(tweet.id)

        println(response.body)
    }


    fun ResponseEntity<String>.assertJson(jsonPath: String): AbstractCharSequenceAssert<*, String> {
        return assertThat(json.from(this.body)).extractingJsonPathStringValue(jsonPath)
    }


    fun TestRestTemplate.graphQl(operationName: String, query: String): ResponseEntity<String> {
        return client.postForEntity("/graphql", GraphQlRequest(operationName, query), String::class)
    }

}

data class GraphQlRequest(
        val operationName: String,
        val query: String,
        val variables: Any? = null
)