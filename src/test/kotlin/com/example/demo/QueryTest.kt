package com.example.demo

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

class QueryTest : SpingBootTest() {

    @Autowired
    lateinit var query: Query

    @Test
    fun `can query tweet by id`() {
        val tweet = aTweet("text")

        val found = query.Tweet(tweet.id!!)!!

        assertThat(found.body).isEqualTo("text")
    }

    @Test
    fun `can query tweets`() {
        val tweet = aTweet("text")

        val found = query.Tweets(("2010-01-01" until "2030-01-01"), null)

        assertThat(found).hasSize(1)
        assertThat(found.first().body).isEqualTo("text")
    }

    @Test
    fun `can query users`() {
        val aUser = aUser(name = "u1")

        val found = query.Users()

        assertThat(found).hasSize(1)
        assertThat(found.first().name).isEqualTo("u1")
    }

}

fun String.asDate(): Date = Date.from(LocalDate.parse(this).atStartOfDay(ZoneId.systemDefault()).toInstant())
infix fun String.until(other: String): TimeRange = TimeRange(this.asDate(), other.asDate())
