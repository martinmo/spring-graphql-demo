package com.example.demo

import org.junit.Before
import org.junit.ClassRule
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.testcontainers.containers.FixedHostPortGenericContainer
import org.testcontainers.containers.wait.strategy.Wait
import java.util.UUID


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = [SpingBootTest.Initializer::class])
abstract class SpingBootTest {

    companion object {
        @ClassRule
        @JvmField
        var mongoContainer = KGenericContainer("mongo:3.7")
//                .withExposedPorts(27017)
                .withFixedExposedPort(27018, 27017)
                .waitingFor(Wait.forListeningPort())
    }

    @Autowired
    lateinit var tweetRepository: TweetRepository

    @Autowired
    lateinit var userRepository: UserRepository

    class Initializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
        override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {
            val values = TestPropertyValues.of(
                    "spring.data.mongodb.uri=mongodb://localhost:27018/tweets"
            )
            values.applyTo(configurableApplicationContext)
        }
    }

    fun aUser(name: String = "a user (${UUID.randomUUID()})"): User {
        return userRepository.save(User(name = name))
    }

    fun aTweet(body: String = "some body", user: User = aUser()): Tweet {
        return tweetRepository.save(Tweet(userId = user.id, body = body))
    }

    @Before
    fun cleanUp() {
        try {
            tweetRepository.deleteAll()
            userRepository.deleteAll()
        } catch (e: Exception) {
            LoggerFactory.getLogger(SpringBootTest::class.java).warn("cleanup failed")
        }
    }
}

// workaround for Kotlin and testcontainers
// https://github.com/testcontainers/testcontainers-java/issues/318
class KGenericContainer(imageName: String) : FixedHostPortGenericContainer<KGenericContainer>(imageName)
