package com.example.demo

import com.coxautodev.graphql.tools.GraphQLSubscriptionResolver
import org.reactivestreams.Publisher
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import reactor.core.publisher.EmitterProcessor
import reactor.core.publisher.Flux
import java.time.Duration
import java.time.ZonedDateTime

@Service
class Subscription(val feed: Feed) : GraphQLSubscriptionResolver {
    fun Time(): Publisher<String> {

        return Flux.interval(Duration.ofSeconds(2)).map {
            val time = ZonedDateTime.now().toString()
            println("time " + time)
            time
        }
    }

    fun Tweets(): Publisher<Tweet> = feed.stream

    fun TweetTexts(): Publisher<String> = feed.stream.map { it.body }
}


@Service
class Feed {
    private val emitter: EmitterProcessor<Tweet> = EmitterProcessor.create()
    val stream: Flux<Tweet> = emitter

    fun next(tweet: Tweet) {
        emitter.onNext(tweet)
    }
}
