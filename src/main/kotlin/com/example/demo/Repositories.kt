package com.example.demo

import org.springframework.data.mongodb.repository.MongoRepository


interface TweetRepository : MongoRepository<Tweet, String>{
    fun findByUserId(userId:String): List<Tweet>
}

interface UserRepository : MongoRepository<User, String>{

}