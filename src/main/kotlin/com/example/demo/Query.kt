package com.example.demo

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.coxautodev.graphql.tools.GraphQLResolver

import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.find
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.stereotype.Service

@Service
class Query(val tweets: TweetRepository, val users: UserRepository, val client: MongoTemplate) : GraphQLQueryResolver {

    fun Tweet(id: String): Tweet? = tweets.findById(id).orElse(null)

    fun Tweets(range: TimeRange, userId: String?): List<Tweet> {
        val criteria = if (userId == null) {
            range.toQueryFilter()
        } else {
            range.toQueryFilter().and("userId").isEqualTo(userId)
        }

        val query = Query.query(criteria)
        return client.find(
                query
        )
    }

    fun Users(): List<User> = users.findAll()

    fun me(): User = users.findAll().first()
}


fun TimeRange.toQueryFilter(): Criteria {
    return where("date").gte(this.from).lt(this.to)
}

@Service
class TweetResolver(val users: UserRepository) : GraphQLResolver<Tweet> {
    fun user(tweet: Tweet): User = users.findById(tweet.userId).orElseThrow { RuntimeException("user not found") }
    fun likedBy(tweet: Tweet): List<User> = users.findAllById(tweet.likesOfUsers).toList()
}

@Service
class UserResolver(val tweetRepository: TweetRepository) : GraphQLResolver<User> {
    fun tweets(user: User): List<Tweet> = tweetRepository.findByUserId(user.id)
}
