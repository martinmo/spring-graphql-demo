package com.example.demo

import org.springframework.stereotype.Component
import java.time.LocalDate
import java.time.ZoneOffset
import java.util.Date
import javax.annotation.PostConstruct

@Component
class Bootstrap(val tweets: TweetRepository, val users: UserRepository) {

    @PostConstruct
    fun init() {

//        users.deleteAll()
        val users = listOf(
                users.save(User(name = "Martin")),
                users.save(User(name = "Martinius"))
        )

        val randomUser: () -> User = { users.shuffled().first() }

//        tweets.deleteAll()
        (1..30).forEach { year ->
            (1..12).forEach { month ->
                val date = LocalDate.of(2000 + year, month, 1)

                tweets.save(Tweet(
                        body = "My tweet on $date",
                        date = Date.from(date.atStartOfDay().toInstant(ZoneOffset.UTC)),
                        userId = randomUser().id
                ))
            }

        }

    }
}