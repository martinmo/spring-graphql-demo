package com.example.demo

import com.coxautodev.graphql.tools.GraphQLMutationResolver
import org.springframework.stereotype.Service

@Service
class Mutation(val users: UserRepository, val tweets: TweetRepository, val feed: Feed) : GraphQLMutationResolver {

    fun createTweet(body: String): Tweet {
        val user = users.findAll().shuffled().first()
        val saved =  tweets.save(Tweet(body = body, userId = user.id))
        feed.next(saved)
        return saved
    }

    fun likeTweet(tweet_id: String): Tweet {
        val user = users.findAll().shuffled().first()
        val tweet = tweets.findById(tweet_id).orElseThrow { RuntimeException("tweet not found") }
        tweet.like(user)
        val saved = tweets.save(tweet)
        feed.next(saved)
        return saved
    }
}
