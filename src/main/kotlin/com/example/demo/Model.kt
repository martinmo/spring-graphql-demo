package com.example.demo

import org.springframework.data.annotation.Id
import java.util.Date
import java.util.UUID

@AllOpen
data class Tweet(
        val body: String,
        @Id val id: String? = null,
        val date: Date = Date(),
        val userId: String,
        val likesOfUsers: MutableSet<String> = mutableSetOf()

) {
    fun getLikes():Long = likesOfUsers.size.toLong()
    fun like(user: User){
        likesOfUsers.add(user.id)
    }
}

@AllOpen
data class User(
        @Id val id: String = UUID.randomUUID().toString(),
        val name: String
)

@AllOpen
data class TimeRange(
        val from: Date,
        val to: Date
)

annotation class AllOpen()